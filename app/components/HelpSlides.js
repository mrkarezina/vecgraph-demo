import {AutoRotatingCarousel, Slide} from './RotatingCarousel';
import PropTypes from 'prop-types';
import React from 'react'

//Used for implementing responsive components
import Responsive from 'react-responsive';

//Layouts for mobile and non-mobile. Mobile=true needs to be set
//Needed to repeat cannot
const Mobile = props => <Responsive {...props} maxWidth={750}/>;
const Default = props => <Responsive {...props} minWidth={751}/>;

import content_map from '../img/full-graph.png'
import click_gif from '../img/click_gif.gif'
import nodes_edges from '../img/d1.png'

const styles = {
    buttonStyle: {
        style: {
            background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
            borderRadius: 3,
            border: 0,
            color: 'white',
            height: 48,
            padding: '0 30px',
            boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        }
    },
    titleStyle: {
        fontSize: '24px',
        fontWeight: 700,
        lineHeight: '32px',
        marginBottom: 12,
        overflow: 'hidden',
        color: '#000000'
    },
    subtitleStyle: {
        fontSize: '16px',
        fontWeight: 400,
        lineHeight: '18px',
        margin: 0,
        color: '#4c4c4c'
    },

};


//Returns the slides with mobile/not mobile prop
function Slides(props) {

    //Try to follow UI guidelines: https://material.io/design/communication/onboarding.html#top-user-benefits-model

    const {mobile, toggle, open} = props;

    return (
        <AutoRotatingCarousel
            open={open}
            onStart={toggle}
            onClose={() => toggle()}
            autoplay={false}
            mobile={mobile}

            //Or a label on each slide
            label={'Exit'}

            //Properties applied to label button
            ButtonProps={
                styles.buttonStyle
            }
        >

            <Slide
                media={<img src={content_map}/>}
                mediaBackgroundStyle={{backgroundColor: '#ffffff'}}
                style={{backgroundColor: '#ffffff'}}
                title={'This is a content map'}
                subtitle='You can use it to discover related content. Each node (circle) represents an article, each article has edges (arrows) that point to its most related articles.'

                //Modified API
                titleStyle={styles.titleStyle}
                subtitleStyle={styles.subtitleStyle}

            />
            <Slide
                media={<img src={click_gif}/>}
                mediaBackgroundStyle={{backgroundColor: '#ffffff'}}
                style={{backgroundColor: '#ffffff'}}
                title={'Exploring the content map'}
                subtitle='Scroll to zoom, click on a node to zoom in on its cluster. Double click anywhere to deselect a cluster.'

                //Modified API
                titleStyle={styles.titleStyle}
                subtitleStyle={styles.subtitleStyle}

            />
            <Slide
                media={<img src={nodes_edges}/>}
                mediaBackgroundStyle={{backgroundColor: '#ffffff'}}
                style={{backgroundColor: '#ffffff'}}
                title='Clusters'
                subtitle='Click on a node to explore its cluster. Different clusters represent closely related articles.'

                //Modified API
                titleStyle={styles.titleStyle}
                subtitleStyle={styles.subtitleStyle}
            />

        </AutoRotatingCarousel>
    )
}

function HelpSlides(props) {
    //Sliding help menu to gives users an overveiw of how to use vecgraph

    const {open, toggle} = props;

    return (
        <div style={{position: 'relative'}}>

            <Mobile>
                <Slides mobile={true} open={open} toggle={toggle}/>
            </Mobile>

            <Default>
                <Slides mobile={false} open={open} toggle={toggle}/>
            </Default>

        </div>
    )
}


HelpSlides.propTypes = {
    toggle: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
};


export default HelpSlides;
