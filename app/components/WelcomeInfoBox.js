import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid"


const styles = theme => ({
    card: {},
    actions: {
        display: 'flex',
    }

});


function WelcomeInfoBox(props) {

    const {classes} = props;

    return (

        <div>
            <Grid container spacing={8} direction={"column"} alignItems={"center"}>
                <Card className={classes.card}>
                    <CardHeader
                        title={"Click any article node to get started."}
                    />

                    <CardContent>
                        <Typography component="h1">
                            {"Double click anywhere to deselect cluster."}
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
        </div>

    )


}

export default withStyles(styles)(WelcomeInfoBox);