import ResponsiveVisualizer from "./ResponsiveVisualizer";
import {getGraphData} from '../utils/api'
import LoadingCircle from './Loading'

import React from "react";
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';


const styles = theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },

});


class ExplorerGraph extends React.Component {
    //Presents the graph vizualization after fetching the correct dataset

    constructor(props) {
        super(props);

        //Show graph will be set to true on enough stablization iterations run
        this.state = {
            data: null,

            //The name of the dataset
            dataset :this.props.dataset
        };
    }

    getNews = async () => {
        this.setState(() => ({
            graphData: null
        }));

        const graphData = await getGraphData(this.state.dataset);
        this.setState(() => ({graphData: graphData}))
    };

    componentDidMount() {
        this.getNews()
    }

    render() {

        //The data loaded from the API call
        const {graphData} = this.state;

        return (

            <div>

                {!graphData
                    ? <LoadingCircle text={["Tip: Click node to zoom in on cluster", "Tip: Click anywhere to zoom out"]}/>
                    : <ResponsiveVisualizer data={graphData} slug={this.props.slug}/>}

            </div>
        )
    }
}

ExplorerGraph.propTypes = {
    classes: PropTypes.object.isRequired,

    //The graph dataset to load
    dataset: PropTypes.string.isRequired,

    //The initally selected title (slug) in the graph explorer
    slug: PropTypes.string,
};

export default withStyles(styles)(ExplorerGraph);