import React from 'react'
import NavBar from './NavBar'
import ExplorerGraph from './ExplorerGraph'
import Grid from "@material-ui/core/Grid"
import HelpSlides from './HelpSlides'
import PropTypes from 'prop-types';


class Explorer extends React.Component {

    /*
        Renders the NavBar that controls if the help menue is visible or not and the Graph.
        Also renders the Graph visulalization
        Checks the match prop (From react router) for the dataset specified in the url
    */

    constructor(props) {
        super(props);

        this.state = {
            showHelp: false,
        };

        this.helpToggle = () => {
            this.setState({
                showHelp: !this.state.showHelp,
            })
        };
    }

    render() {

        const {dataset, slug} = this.props;

        return (
            <div>

                <Grid container spacing={24} direction={"column"}>

                    <Grid item xs={12}>
                        <NavBar toggle={this.helpToggle}/>
                    </Grid>

                    <Grid item xs={12}>
                        <ExplorerGraph dataset={dataset} slug={slug}/>
                        <HelpSlides open={this.state.showHelp} toggle={this.helpToggle}/>
                    </Grid>

                </Grid>

            </div>

        )
    }

}

Explorer.propTypes = {

    //The name of the vis.js dataset fetch from the API
    dataset: PropTypes.string.isRequired,

    //The initally selected title (slug) in the graph explorer
    slug: PropTypes.string,
};

export default Explorer;

