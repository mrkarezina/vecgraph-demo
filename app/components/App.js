import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Welcome from './WelcomePage'

class App extends React.Component {

    /*

        Structure:
        The Welcome component parses the dataset name from the url

        NOTE: Specify the default dataset in the Welcome page component.

        Home page calls Welcome component and if no dataset specified in URL defualt in used.
        After the OnboardingSlides slides are shown, app redirects to explorer using the dataset passed to it
        The explorer route parses url and renders the graph explorer
        The Explorer component then fetches the right dataset and displays the vizualizer.
     */

    render() {

        return (

            <Router>
                <div>

                    <Switch>

                        {/*Used for directly getting to a specific dataset*/}
                        <Route exact path='/:dataset' component={Welcome}/>
                        <Route exact path='/:dataset/:slug' component={Welcome}/>

                        {/*Default dataset when going to home page. Can change default in Welcome*/}
                        <Route exact path='/' component={Welcome}/>


                        {/*404 catch all*/}
                        <Route component={Welcome}/>
                    </Switch>

                </div>
            </Router>

        )
    }

}

export default App;