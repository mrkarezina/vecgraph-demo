import React from 'react'
import {Redirect} from 'react-router-dom'
import OnboardingSlides from './OnboardingSlides'
import NavBar from './NavBar'
import Explorer from './Explorer'
import PropTypes from 'prop-types'

class Welcome extends React.Component {

    /*
        Shows the OnboardingSlides slides (The intro)
        Checks the match prop (From react router) for the dataset specified in the url
        Uses that url to redirect to /explorer/the_dataset (And render same dataset that was specified to the Welcome Component)
     */

    constructor(props) {
        super(props);
        this.state = {
            showHelp: true
        };

        this.helpToggle = () => {
            this.setState({
                showHelp: !this.state.showHelp,
            })
        };

        this.defaultDataset = "techcrunch"

    }

    //Try to get the dataset from url, if undefined return default dataset
    getDataset() {
        const dataset = this.props.match.params.dataset;
        if (!dataset) {
            return this.defaultDataset
        } else {
            return dataset
        }
    }

    //The initally selected title (slug) in the graph explorer (OPTIONAL)
    getTitleSlug() {
        const slug = this.props.match.params.slug;
        if (!slug) {
            return '';
        } else {
            return slug;
        }
    }

    render() {

        const dataset = this.getDataset();
        const slug = this.getTitleSlug();

        return (
            <div>

                <NavBar toggle={this.helpToggle}/>

                <OnboardingSlides toggle={this.helpToggle} open={this.state.showHelp}/>
                {this.state.showHelp
                    ? <OnboardingSlides toggle={this.helpToggle} open={this.state.showHelp}/>
                    : <Explorer dataset={dataset} slug={slug}/>}
            </div>

        )
    }


}

Welcome.propTypes = {

    //Should contain the dataset property
    match: PropTypes.object.isRequired,
};

export default Welcome;

