import {AutoRotatingCarousel, Slide} from './RotatingCarousel';
import PropTypes from 'prop-types';
import React from 'react'

//Used for implementing responsive components
import Responsive from 'react-responsive';

//Layouts for mobile and non-mobile. Mobile=true needs to be set
//Needed to repeat cannot
const Mobile = props => <Responsive {...props} maxWidth={750}/>;
const Default = props => <Responsive {...props} minWidth={751}/>;

import content_map from '../img/full-graph.png'
import slide from '../img/slide.gif'
import click_gif from '../img/click_gif.gif'
import circles_arrows from '../img/d1.png'


const styles = {
    buttonStyle: {
        style: {
            background: '#7cc9ff',
            borderRadius: 3,
            border: 0,
            color: 'white',
            height: 48,
            padding: '0 30px',
        }
    },
    titleStyle: {
        fontSize: '24px',
        fontWeight: 700,
        lineHeight: '32px',
        marginBottom: 12,
        overflow: 'hidden',
        color: '#000000'
    },
    subtitleStyle: {
        fontSize: '16px',
        fontWeight: 400,
        lineHeight: '18px',
        margin: 0,
        color: '#4c4c4c'
    },

};

//Returns the slides with mobile/not mobile prop
function Slides(props) {

    /*
        RotatingCarousel is a copy of the: https://mui.wertarbyte.com/#material-auto-rotating-carousel
        The carosel is build on the material UI was modified becuse the material-auto-rotating-carousel API was not working
        Modified the Style in the Slide.js to customize

        Try to follow UI guidelines: https://material.io/design/communication/onboarding.html#top-user-benefits-model
     */
    const {mobile, toggle, open} = props;

    return (
        <AutoRotatingCarousel
            open={open}
            onStart={toggle}
            onClose={() => toggle()}
            autoplay={true}
            interval={8000}
            mobile={mobile}

            //Or a label on each slide
            label={'Get Started'}

            //Properties applied to label button
            ButtonProps= {
                styles.buttonStyle
            }
        >

            <Slide
                media={<img src={slide}/>}
                mediaBackgroundStyle={{backgroundColor: '#ffffff'}}
                style={{backgroundColor: '#ffffff'}}
                title={'Slide to explore'}
                subtitle='Use the slider to explore the content map'

                //Modified API
                titleStyle={styles.titleStyle}
                subtitleStyle={styles.subtitleStyle}
            />

            <Slide
                media={<img src={click_gif}/>}
                mediaBackgroundStyle={{backgroundColor: '#ffffff'}}
                style={{backgroundColor: '#ffffff'}}
                title={'Click or double click'}
                subtitle='Click circle (article) once to select it, double click to deselect.'

                //Modified API
                titleStyle={styles.titleStyle}
                subtitleStyle={styles.subtitleStyle}
            />

            <Slide
                media={<img src={content_map}/>}
                mediaBackgroundStyle={{backgroundColor: '#ffffff'}}
                style={{backgroundColor: '#ffffff'}}
                title={'This is a content map'}
                subtitle='You can use it to explore and discover new content.'

                //Modified API
                titleStyle={styles.titleStyle}
                subtitleStyle={styles.subtitleStyle}
            />


            <Slide
                media={<img src={slide}/>}
                mediaBackgroundStyle={{backgroundColor: '#ffffff'}}
                style={{backgroundColor: '#ffffff'}}
                title={'Scroll to zoom'}
                subtitle='You can scroll to zoom in or out.'

                //Modified API
                titleStyle={styles.titleStyle}
                subtitleStyle={styles.subtitleStyle}
            />

            <Slide
                media={<img src={circles_arrows}/>}
                mediaBackgroundStyle={{backgroundColor: '#ffffff'}}
                style={{backgroundColor: '#ffffff'}}
                title={'Circles and arrows'}
                subtitle='Circles represent articles, arrows represent relations between articles.'

                //Modified API
                titleStyle={styles.titleStyle}
                subtitleStyle={styles.subtitleStyle}
            />

        </AutoRotatingCarousel>
    )
}

function OnboardingSlides(props) {
    //Sliding help menu to gives users an overveiw of how to use vecgraph

    const {open, toggle} = props;

    return (
        <div style={{position: 'relative'}}>

            <Mobile>
                <Slides mobile={true} open={open} toggle={toggle}/>
            </Mobile>

            <Default>
                <Slides mobile={false} open={open} toggle={toggle}/>
            </Default>

        </div>
    )
}


OnboardingSlides.propTypes = {
    toggle: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
};


export default OnboardingSlides;
