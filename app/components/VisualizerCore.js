/*
       CAUTION: Kind of hacky works by binding network in react-graph-vis to component
 */

import PropTypes from 'prop-types';
import React from "react";

//How many nodes to add remove every slide of the exploration slider
const nodesPerExploration = 3;

class VisualizerCore extends React.Component {

    /**
     Graph core is used as the base class for both the Mobile, and desktop graph Components
     The graphcore contains the logic for interfacing with the react-network component
     as well as updating the state with the selected node data used in the subclasses (the render infobox)

     All functions use the node labels. Only when setting the selected, and selectedQueue states is the full data fetched
     */

    constructor(props) {
        super(props);
        this.state = {

            //The info of currently selected node, set to null to make sure Welcome info-box rendered
            //First index of exploreSelected contains selected
            //TODO: kind of unessecary could use exploreSelected[0]
            selected: null,

            //The list of nodes that are connected to a selected node, the queue grows as the users increases the value of the slider and there are no nodes in the queue,
            //Otherwise nodes transferee from queue to explore selected (The ones to be rendered)
            exploreQueue: [],
            exploreSelected: [],

            //Updated by a callback passed to slider components in the Visualizer Child components.
            //Keeps track of the index in explore queue, explorationLevel*nodesPerExploration nodes moved into exploreSelected
            explorationLevel: 1,

            network: [],
            nodeData: this.props.data.nodes,
            showGraph: false,

            //Will disable physic and other stuff to improve performance.
            physicsDisabled: this.props.physicsDisabled,
        };

        //Bind functions becuase they are passed down as callbacks to network component
        this.nodeSelectedGraph = this.nodeSelectedGraph.bind(this)
        this.nodeSelectInfoBox = this.nodeSelectInfoBox.bind(this)
        this.showGraph = this.showGraph.bind(this)
        this.showAll = this.showAll.bind(this)

        //Bind function which is passed down to Slider component (Used in Visualizer Child components.)
        this.sliderChange = this.sliderChange.bind(this)
    }

    //Called when the react-vis.js graph component is rendered in the Child classes, and sets the network state parameter
    onGraphComponentLoad(network) {

        this.setState({network: network})
    }


    //Called after stabilization iterations are run
    showGraph() {

        //Initial selected node can be passed as arg
        //Ignore if the url slug is incorrect
        if (this.props.initialSelected) {
            try {
                const label = this.getLabelfromTitleId(this.props.initialSelected);

                this.nodeSelectInfoBox(label)
            } catch (e) {
                null;
            }
        }

        this.setState({
            showGraph: true,
        });

        //Disable physics
        if (this.state.physicsDisabled) {
            this.state.network.physics.stabilized = true;
            this.state.network.physics.physicsEnabled = false;
        }

    }


    //Updates the current seleced nodes, and ones in queue
    updateSelected(original, connected) {

        let articleData = this.getNodeData(original);

        articleData.connected = connected;

        //Get full data of connected nodes, connected is object with label, and weight
        let connectedData = connected.map((conn) => {
            return this.getNodeData(conn.label)
        });

        let exploreSelected = [articleData];
        exploreSelected.push(...connectedData);

        this.setState({
            selected: articleData,

            //To place an inital articles (the selected and the directly connected) into selected feed to be seen
            exploreSelected: exploreSelected,
        })
    }

    //Passed down as callback to slider component,
    //Function uses a queue of titles and inserts the titles into the selected array depending on the "Explore" value of slider
    //Inital value of Queue are the titles in the cluster
    sliderChange(sliderValue) {

        //Inital value of Queue are the titles in the cluster
        let exploreQueue = this.state.exploreQueue;

        //If zero explore level 1 (When sliding all way back), only inclucde single title in queue
        if (sliderValue === 1) {
            exploreQueue = [this.state.selected.label]
        }

        //If the queue is not big enough fill it with the nodes connected to the nodes currently selected
        if (exploreQueue.length <= nodesPerExploration * sliderValue) {

            let newQueue = [];
            exploreQueue.forEach((nodeId) => {

                //Get connected nodes and push them to the queue
                this.state.network.getConnectedNodes(nodeId).forEach((connectNode) => {
                    newQueue.push(connectNode)
                });
            });

            //Extend array
            exploreQueue.push(...newQueue)
            //Take unique values
            exploreQueue = [...new Set(exploreQueue)];

        }

        //Get the data for each of the titles in the queue
        let exploreSelected = exploreQueue.slice(0, nodesPerExploration * sliderValue)

        this.highlightNodes(exploreSelected)

        exploreSelected = exploreSelected.map((label) => {
                return this.getNodeData(label);
            }
        );

        this.setState({
            explorationLevel: sliderValue + 1,
            exploreQueue: exploreQueue,
            exploreSelected: exploreSelected,
        });

    };

    sliderExplorationReset() {
        //Used to reset the exploration slider on click

        this.setState(
            {
                exploreQueue: [],
                exploreSelected: [],
                explorationLevel: 1,
            }
        )
    }


    //When a node is clicked on update state of the selected node and state with info from that node (for infobox)
    //Updates the selected state with n nodes from cluster, and adds the nodes in the cluster to Queue
    selectionHandler(nodeId) {

        this.sliderExplorationReset();
        const connectedNodes = this.state.network.getConnectedNodes(nodeId);

        let withWeight = [];

        //Sorts the connections by weight, if not connected 0 weight
        connectedNodes.forEach((connect) => {

            withWeight.push({
                label: connect,
                weight: this.getWeight(nodeId, connect)
            })
        });

        withWeight = withWeight.sort(function (a, b) {
            return b.weight - a.weight;
        });

        //So only show top n nodes initally
        withWeight = withWeight.slice(0, nodesPerExploration - 1);

        this.updateSelected(nodeId, withWeight);

        //Highlight the node labels
        const withoutWeight = this.nodeLabelsFromData(withWeight);
        withoutWeight.push(nodeId);
        this.highlightNodes(withoutWeight);

        //Add the node labels to queue, place the selected as first in queue
        connectedNodes.unshift(nodeId);

        //Need to use setstate or does not work when selected from infobox
        this.setState({
            exploreQueue: connectedNodes,
        })

    }

    //Function is passed to Vizualizer.js, and infoBox component to update the selected node
    nodeSelectedGraph(params) {

        const title = params.nodes[0];
        this.selectionHandler(title);

        this.state.network.focus(title, {
            scale: 0.85,
        })

    }

    //Call back for when a title is selected form the infobox
    nodeSelectInfoBox(nodeId) {

        //Selects the node as if it where clicked on
        this.state.network.selectNodes([nodeId])

        this.state.network.focus(nodeId, {
            scale: 0.85,
        });

        this.selectionHandler(nodeId)
    }


    /**
     Following functions used for highlighting and un-highlighting the graph
     */

    //Highlights a list of nodes lables
    highlightNodes(nodes) {

        //If physics is enabled need to disable when selecting
        if (!this.state.physicsDisabled) {
            this.state.network.physics.stabilized = true;
            this.state.network.physics.physicsEnabled = false;
        }

        const allNodes = this.props.data.nodes;

        //Reset to all hidden
        for (let i = 0; i < allNodes.length; i++) {
            allNodes[i].hidden = true;
        }

        //Get target nodes, and then unhide them
        let filtered = allNodes.filter((node) => {
            if (nodes.includes(node.label)) {
                return node
            }
        }).map((node) => {
            node.hidden = false;
            return node
        });

        this.state.network.body.data.nodes.update(allNodes);
        this.state.network.body.data.nodes.update(filtered);


    }

    //Set all other nodes to invisible apart from ones with the same group label
    highlighCluster(nodeId) {

        //If physics is enabled need to disable when selecting
        if (!this.state.physicsDisabled) {
            this.state.network.physics.stabilized = true;
            this.state.network.physics.physicsEnabled = false;
        }

        const allNodes = this.props.data.nodes;
        const groupNodes = this.getNodesInCluster(nodeId)

        let hideNodes = [];
        for (let i = 0; i < allNodes.length; i++) {

            let node = allNodes[i];

            if (groupNodes.includes(node.label) || (node.label === nodeId)) {
                node.hidden = false
                hideNodes.push(node)

            } else {
                node.hidden = true
                hideNodes.push(node)
            }
        }

        this.state.network.body.data.nodes.update(hideNodes);

    }


    //Call back for when double click to deselect node
    showAll() {

        //If physics is not disabled then need to re-enable physics
        if (!this.state.physicsDisabled) {
            this.state.network.physics.physicsEnabled = true;
        }

        const allNodes = this.props.data.nodes;
        let hideNodes = [];
        let previouslyHidden = [];

        for (let i = 0; i < allNodes.length; i++) {

            let node = allNodes[i]

            //Collect previously show to be able to zoom on them.
            if (node.hidden !== true) {
                previouslyHidden.push(node)
            }

            node.hidden = false;
            hideNodes.push(node)
        }

        //TODO: keep zoom_gif constant
        //Show all the nodes, and restabilize graph if the physics is disabled
        this.state.network.body.data.nodes.update(hideNodes);


        //Run this to zoom_gif out to full view:
        // this.state.network.fit()

    }


    /**
     Following functions are Utility funcitons
     */

    getLabelfromTitleId(titleId) {
        const filtered = this.state.nodeData.filter((rel) => {
            return rel.titleId === titleId
        })

        return filtered[0].label
    }


    //Takes node label, and returns list of node labels that are in cluster not including orignal node
    getNodesInCluster(node) {

        const group = this.getNodeGroupLabel(node)

        const allNodes = this.props.data.nodes;

        let groupNodes = allNodes.filter((n) => {

            //Not to include the original node
            if ((n.group === group) && (n.label !== node)) {
                return n
            }
        }).map((n) => {

            //To turn into a node data object
            return n.label
        });

        return groupNodes

    }

    //Gets the group label of node
    getNodeGroupLabel(node) {
        const foundNode = this.props.data.nodes.filter((n) => {
            return n.label === node
        })

        return foundNode[0].group
    }

    //Gets the weight of the edge connecting two nodes
    //If the nodes are not connected returns 0 weight
    //Uses the edge key property set in the dataset which is just the titles concated together
    getWeight(nodeId1, nodeId2) {

        const edgeKey1 = nodeId1 + nodeId2
        const edgeKey2 = nodeId2 + nodeId1

        const relation = this.props.data.edges.filter((rel) => {
            return rel.edgeKey === edgeKey1 || rel.edgeKey === edgeKey2
        });

        if (relation.length === 0) {
            return 0
        } else {
            return relation[0].fakeWeight
        }
    }

    //Queries to find the link and intro for the node specified node
    //Filters and formats the node data using map
    //Returns object with data
    getNodeData(nodeID) {

        const filtered = this.state.nodeData.filter((rel) => {
            return rel.label === nodeID
        }).map((rel) => {
                let {label, url, img_url, date} = rel;

                date = new Date(date).toDateString();

                return {
                    label: label,
                    link: url,
                    img_link: img_url,
                    date: date,
                }
            }
        );

        return filtered[0]
    }

    //Converts node data list into node labels list
    nodeLabelsFromData(nodeLabels) {

        return nodeLabels.map((node) => {
            return node.label
        })

    }

}

VisualizerCore.propTypes = {
    data: PropTypes.object.isRequired,

    //Specify initial selected node
    initialSelected: PropTypes.string,
    physicsDisabled: PropTypes.bool.isRequired
}


export default VisualizerCore;
