import React from "react";
import PropTypes from 'prop-types'
import {Slider, Rail, Handles, Tracks, Ticks} from "react-compound-slider";
import {MuiRail, MuiHandle, MuiTrack, MuiTick} from "./muiComponents"; // example render components - source below

// Based on Material Design spec:
// Styles by https://github.com/RafeSacks
// https://github.com/sghall/react-compound-slider/issues/41
// https://material.io/design/components/sliders.html#spec

class MuiSlider extends React.Component {
    constructor(props) {
        super(props);

        //The initial value of the circle
        const initialValues = [this.props.initialValue];

        this.updateCallback = this.props.updateCallback;

        this.state = {
            domain: [1, 10],
            values: [...initialValues],
            update: [...initialValues]
        };
    }

    //During a drag
    onUpdate = update => {

        //Updates the value of the slider if new "tick" reached
        //Pass value into update callback
        if (update[0] !== this.state.update[0]) {
            this.setState({update});
            this.updateCallback(update[0])
        }

    };

    //At the end of a drag
    onChange = values => {
        this.setState({values});
    };


    render() {
        const {domain, values, update} = this.state;

        return (
            <div style={{paddingTop: "2vh", paddingBottom: "4vh", paddingLeft: "2vh", paddingRight: "2vh"}}>
                <Slider
                    step={1}
                    domain={domain}
                    rootStyle={{
                        position: "relative",
                        width: "100%"
                    }}
                    onUpdate={this.onUpdate}
                    onChange={this.onChange}
                    values={values}
                >

                    <Rail>
                        {({getRailProps}) => <MuiRail getRailProps={getRailProps}/>}
                    </Rail>

                    <Handles>
                        {({handles, getHandleProps}) => (
                            <div className="slider-handles">
                                {handles.map(handle => (
                                    <MuiHandle
                                        key={handle.id}
                                        handle={handle}
                                        domain={domain}
                                        getHandleProps={getHandleProps}
                                    />
                                ))}
                            </div>
                        )}
                    </Handles>

                    <Tracks left={true} right={false}>
                        {({tracks, getTrackProps}) => (
                            <div className="slider-tracks">
                                {tracks.map(({id, source, target}) => (
                                    <MuiTrack
                                        key={id}
                                        source={source}
                                        target={target}
                                        getTrackProps={getTrackProps}
                                    />
                                ))}
                            </div>
                        )}
                    </Tracks>

                    <Ticks count={10}>
                        {({ticks}) => (
                            <div className="slider-ticks">
                                {ticks.map(tick => (
                                    <MuiTick key={tick.id} tick={tick} count={ticks.length}/>
                                ))}
                            </div>
                        )}
                    </Ticks>
                </Slider>
            </div>
        );
    }
}

MuiSlider.propTypes = {
    initialValue: PropTypes.number.isRequired,
    updateCallback: PropTypes.func.isRequired,
};


export default MuiSlider;

