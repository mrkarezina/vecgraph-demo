import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import Typography from '@material-ui/core/Typography';

import Grid from "@material-ui/core/Grid"


const styles = {

    actions: {
        display: 'flex',
        alignItems: 'center'
    },
    readfull: {},

    card: {
        minWidth: 275,
    },
    title: {
        fontFamily: 'Roboto',
        fontSize: 16,
        fontWeight: 500,
        color: '#1e1e1e',

    },
    pos: {
        fontSize: 10,
        marginBottom: 8,
    },
    image: {
        width: '100%',
        backgroundSize: 'auto 100%',
        backgroundPosition: 'center center',
    },
    exploreButton: {
        verticalAlign: 'bottom',
        textAlign: 'right',
        fontSize: 10,
        paddingRight: 5,
    }

};


function ArticleCard(props) {

    const {classes, articleData, selectNodeHandler} = props;

    const {label, link, img_link, date} = articleData;
    //TODO: update image

    return (
        <Card className={classes.card} raised={false}>

            <Grid container spacing={24}>

                <Grid item xs={7}>
                    <CardActionArea component="a" href={link} target="_blank" rel="nofollow">
                        <CardContent>
                            <Typography variant="inherit" className={classes.title}>
                                {label}
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                {date}
                            </Typography>

                        </CardContent>
                    </CardActionArea>
                </Grid>


                <Grid item xs={5}>
                    <img className={classes.image}
                         src={img_link}/>

                    <CardActionArea onClick={() => {
                        selectNodeHandler(label)
                    }}>
                        <Typography className={classes.exploreButton} color="textSecondary">
                            Explore
                        </Typography>
                    </CardActionArea>

                </Grid>

            </Grid>

        </Card>
    )

}

ArticleCard.propTypes = {
    classes: PropTypes.object.isRequired,
    articleData: PropTypes.shape({
        label: PropTypes.string.isRequired,
        link: PropTypes.string.isRequired,
        img_link: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
    }),
    selectNodeHandler: PropTypes.func.isRequired,
};


export default withStyles(styles)(ArticleCard);