import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Grid from "@material-ui/core/Grid"

import ArticleCard from './ArticleCard'


const styles = {
    message: {
        fontFamily: 'Roboto',
        fontSize: 16,
        fontWeight: 500,
        color: '#1e1e1e',
        textAlign: 'center',

    },

    feed: {

        //-40 px = 5*spacing.unit which is size of toolbar
        //100% height does not work for some reason
        maxHeight: 'calc(100vh - 115px)',
        overflow: 'auto',
        flexGrow: 1,
        flexDirection: 'column'
    }
};


class ArticleFeed extends React.Component {

    render() {
        const {classes, articles, selectNodeHandler} = this.props;

        return (
            <div>
                <Grid container spacing={8} direction={"column"} alignItems={"center"}>

                    <div>
                        <List className={classes.feed}>

                            <Typography variant="inherit" className={classes.message}>
                                Scroll slider for more
                            </Typography>

                            {articles.map(function (articleData) {

                                return (
                                    <ListItem key={articleData.label}>
                                        <ArticleCard selectNodeHandler={selectNodeHandler} articleData={articleData}/>
                                    </ListItem>
                                )
                            })}

                            <Typography variant="inherit" className={classes.message}>
                                Scroll slider for more
                            </Typography>

                        </List>

                    </div>

                </Grid>
            </div>
        );
    }
}

ArticleFeed.propTypes = {
    classes: PropTypes.object.isRequired,
    articles: PropTypes.array.isRequired,
    selectNodeHandler: PropTypes.func.isRequired,

};

export default withStyles(styles)(ArticleFeed);
