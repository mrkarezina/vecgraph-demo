/*
       CAUTION: Kind of hacky works by binding network in react-graph-vis to component
 */


import PropTypes from 'prop-types';
import React from "react";

import DesktopVisualizer from './DesktopVisualizer'
import MobileVisualizer from './MobileVisualizer'

//Used for implementing responsive components
import Responsive from 'react-responsive';

const Mobile = props => <Responsive {...props} maxWidth={1100}/>;
const Default = props => <Responsive {...props} minWidth={1101}/>;


function ResponsiveVisualizer(props) {
    //Used to render the physics on of off depending on screen size
    //Will cause the graph to re-render if the screen size is changed

    const {data, slug} = props;

    return (
        <div>

            <Default>
                <DesktopVisualizer data={data} physicsDisabled={false} initialSelected={slug}/>
            </Default>

            <Mobile>
                <MobileVisualizer data={data} physicsDisabled={true} initialSelected={slug}/>
            </Mobile>

        </div>
    )

}

ResponsiveVisualizer.propTypes = {
    data: PropTypes.object.isRequired,

    //The initally selected title (slug) in the graph explorer
    slug: PropTypes.string,
};


export default ResponsiveVisualizer;
