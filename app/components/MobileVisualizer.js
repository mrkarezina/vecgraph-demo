/*
       CAUTION: Kind of hacky works by binding network in react-graph-vis to component
 */

import VisualizerCore from './VisualizerCore'

import Graph from "react-graph-vis";
import PropTypes from 'prop-types';
import React from "react";

import {ArticleFeed} from './ArticleFeed'
import {ExplorationSlider} from './ExplorationSlider'

import WelcomeInfoBox from "./WelcomeInfoBox"
import LoadingCircle from './Loading'
import Grid from "@material-ui/core/Grid"

import {withStyles} from '@material-ui/core/styles';


const styles = theme => ({
    container: {
        paddingTop: '3px',
        paddingLeft: '3px',
        paddingRight: '3px'
    }
});


class MobileVisualizer extends VisualizerCore {

    settings = {
        options: {
            autoResize: true,

            //Width seems to work fine
            //-40 px = 5*spacing.unit which is size of toolbar
            //Can't just use 100% height
            height: 'calc(50vh - 10px)',

            nodes: {
                shape: 'dot',
                mass: 2,

                //For the wrapping of the node labels
                widthConstraint: {
                    maximum: 85
                },

                font: {
                    size: 12,
                    color: '#1e1e1e',
                    face: 'Roboto',
                },
                borderWidth: 2,

                //Highlights the selected node
                chosen: {
                    node: function (values, id, selected, hovering) {
                        values.color = '#ffffff';
                        values.borderColor = '#1e1e1e';
                    },
                    label: function (values, id, selected, hovering) {
                        values.mod = 'bold';
                    }
                }
            },

            edges: {
                smooth: {
                    enabled: false,
                    type: "vertical"
                },

                width: 2,

                //Highlights the edges when chosen
                chosen: {
                    edge: function (values, id, selected, hovering) {
                        values.color = '#565656';
                    }
                },
            },

            physics: {
                forceAtlas2Based: {
                    gravitationalConstant: -50,
                    centralGravity: 0.01,
                    springConstant: 0.3,
                    springLength: 150,
                    damping: 0.4,
                    avoidOverlap: 0.5
                },
                stabilization: {
                    enabled: true,
                    iterations: 300,
                },
                solver: "forceAtlas2Based",
                minVelocity: 2.5,
            },

            layout: {
                randomSeed: 100,
                improvedLayout: false,
            },

            interaction: {
                tooltipDelay: 200
            }
        },

        style: {
            border: "1px solid black",
            borderRadius: "9px",
            borderColor: "#d5d7db",
        }

    };

    render() {

        const events = {
            "selectNode": this.nodeSelectedGraph,
            "doubleClick": this.showAll,
            "stabilizationIterationsDone": this.showGraph,
        };

        const {classes} = this.props;

        return (

            <div>
                <Grid container spacing={24} className={classes.container}>
                    <Grid item xs={12}>

                        <Graph graph={this.props.data} options={this.settings.options} events={events}
                               style={this.settings.style}
                               getNetwork={network => this.onGraphComponentLoad(network)}/>

                        {!this.state.selected
                            ? null
                            //Key given to slider so when the selected title changes the slider resets
                            : <ExplorationSlider updateCallback={this.sliderChange} initialValue={this.state.explorationLevel} key={this.state.selected.label}/>
                        }

                    </Grid>

                    <Grid item xs={12}>

                        {!this.state.showGraph
                            ? <LoadingCircle text={["Generating graph..."]}/>
                            : null}

                        {!this.state.selected
                            ? <WelcomeInfoBox/>
                            : <div>
                                <ArticleFeed selectNodeHandler={this.nodeSelectInfoBox}
                                             articles={this.state.exploreSelected}/>
                            </div>
                        }

                    </Grid>


                </Grid>

            </div>
        )
    }
}

MobileVisualizer.propTypes = {
    classes: PropTypes.object.isRequired,
    data: PropTypes.object.isRequired,
    physicsDisabled: PropTypes.bool.isRequired
};

export default withStyles(styles)(MobileVisualizer);
