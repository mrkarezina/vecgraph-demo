import axios from 'axios'

async function getGraphJSON(dataset) {
    return await axios.get(`https://graph-intelligence.appspot.com/demo?data_set=${dataset}`);
    // return await axios.get(`http://127.0.0.1:5000/demo?data_set=cnn`);
}

// Slugify a string
function convertToSlug(str) {

    //Used to add an titleId property to the nodes, which is just the slugified title.
    //This id is used to identify the initial node to select.

    str = str.replace(/^\s+|\s+$/g, '');

    // Make the string lowercase
    str = str.toLowerCase();

    // Remove accents, swap ñ for n, etc
    let from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
    let to = "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
    for (let i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    // Remove invalid chars
    str = str.replace(/[^a-z0-9 -]/g, '')
    // Collapse whitespace and replace by -
        .replace(/\s+/g, '-')
        // Collapse dashes
        .replace(/-+/g, '-');

    return str;
}


//Adds a key of the two articles to the weight of the relationship
function addWeightProperty(graphData) {

    //[0] becuase of how google storage returned a list of json
    graphData.data = graphData.data[0];

    graphData.data.nodes = graphData.data.nodes.map((obj) => {
        obj.titleId = convertToSlug(obj.label);
        return obj;
    });

    graphData.data.edges = graphData.data.edges.map((obj) => {
        obj.edgeKey = obj.to + obj.from;

        //This is done so the graph does not have weighted edges
        obj.fakeWeight = obj.value
        delete obj.value

        return obj;
    });

    return graphData
}

export async function getGraphData(dataset) {
    const [graphData] = await Promise.all([
        getGraphJSON(dataset)
    ]);

    return addWeightProperty(graphData).data
}